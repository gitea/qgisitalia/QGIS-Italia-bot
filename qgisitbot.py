#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
****************************************************************************
qgisitbot.py
                      -------------------
begin                : 2016-03-07
copyright            : (C) 2016 by Salvatore Larosa
email                : lrssvtml (at) gmail (dot) com
****************************************************************************
"""

from random import getrandbits

import re
# import xml.dom.minidom as minidom
from xml.etree import ElementTree
# import xml.sax.saxutils as saxutils
# import htmllib

import sys
import os

sys.path.append("/Users/larosa/dev/qgisitbot/ext-libs/")

from telegram import Updater, Update, InlineQueryResultArticle, ParseMode
import urllib2
from bs4 import BeautifulSoup
import logging

GROUP = "5250612"

# Enable logging
logging.basicConfig(
        filename=os.path.join(os.path.expanduser('~/error_landslidesbot.log')),
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

logger = logging.getLogger("qgisitbot")
last_chat_id_g = 0


def remove_tags(text):
    return ''.join(ElementTree.fromstring(text).itertext())

def html_decode(s):
    """
    Returns the ASCII decoded version of the given HTML string. This does
    NOT remove normal HTML tags like <p>.
    """
    htmlCodes = (
            ("'", '&#39;'),
            ('"', '&quot;'),
            ('>', '&gt;'),
            ('<', '&lt;'),
            ('&', '&amp;')
        )
    for code in htmlCodes:
        s = s.replace(code[1], code[0])
    return s

def get_content_from_gmane(query_string):
    sanitize_query = '+'.join(query_string.split())

    page = "http://osgeo-org.1560.x6.nabble.com/template/NamlServlet.jtp?macro=search_page&node={1}&query={0}&i=0".format(
        sanitize_query, GROUP)
    content = urllib2.urlopen(page).read()
    soup = BeautifulSoup(content, "html.parser")
    items = soup.find_all('div', attrs={'class': 'adbayes-content'})
    footer = soup.find_all('div', attrs={'class': 'weak-color'})

    return items, footer, page

def inline_query(bot, update):
    if update.inline_query is not None and update.inline_query.query:
        query = update.inline_query.query
        results = list()

        items, footer, page = get_content_from_gmane(query)

        if not items:
            results.append(InlineQueryResultArticle(
                    id=hex(getrandbits(64))[2:],
                    title="Nessun risultato",
                    message_text="Nessun risultato, prova a cambiare il testo da ricercare.",
                    description="Prova a cambiare il testo da ricercare.",
                    thumb_url='http://lrssvt.ns0.it/qgisitbot/sad.png'))
        else:
            it = iter(items)
            cnt = 0
            for title in it:
                text = next(it)
                author = remove_tags(str(footer[cnt].find_all('a')[1]))
                description = html_decode(remove_tags(str(text))).strip().replace('\n', ' ')
                results.append(InlineQueryResultArticle(
                        id=hex(getrandbits(64))[2:],
                        title=u' - '.join([html_decode(remove_tags(str(title.span.a))), author]),
                        message_text=u'\n'.join(["*Oggetto:* " + remove_tags(str(title.span.a)),
                                                "*Autore:* " + author,
                                                "[Vai al messaggio](" + title.span.a["href"] + ")",
                                                "[Visualizza l'intera ricerca](" + page + ")"]),
                        parse_mode=ParseMode.MARKDOWN,
                        disable_web_page_preview=True,
                        description=description,
                        thumb_url='http://lrssvt.ns0.it/qgisitbot/qgisitbot.png'))
                cnt += 1

        bot.answerInlineQuery(update.inline_query.id, results=results)


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))

def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater(TOKEN_BOT)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on noncommand i.e message - echo the message on Telegram
    dp.addTelegramInlineHandler(inline_query)

    # log all errors
    dp.addErrorHandler(error)

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    main()
